# Natta - an iPhone nightstand app

## How to use 

*To change color*

Tap the clock

*To change the intensity of the light*

Slide up and down.

*To change text size*

Pinch

## Privacy Policy

This app does not gather or store any kind of personal data, apart from what Apple automatically collects through the App Store. 

Therefore, there are no personal data for me to use, misuse, sell, or do anything else with. Not that I would if I could, but I cant. 

Just like it should be.
