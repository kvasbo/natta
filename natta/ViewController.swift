//
//  ViewController.swift
//  clock
//
//  Created by Audun Kvasbø on 04.03.2018.
//  Copyright © 2018 Rettvendt AS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // Bindings
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var mainDisplay: UIStackView!
    @IBOutlet weak var verticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var batteryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    // Config
    let maxSize = 130;
    let smallTextFactor = 0.44
    let colors = ["#FFFFFF", "#BF2A2A", "#E3CDA4", "#04A1C4", "#00C73A"]
    
    // System stuff
    var clockTimer = Timer()
    let langCode = Locale.current.languageCode!
    let regionCode = Locale.current.regionCode!
    let timeFormatter = DateFormatter()
    let dateFormatter = DateFormatter()
    let batteryFormatter = NumberFormatter()
    
    // App specific stuff
    var locale = "en-US"
    let moveDuration = TimeInterval(600)
    var moveConstant = CGFloat(0)
    var alpha = (UserDefaults.standard.value(forKey: "alpha") as? CGFloat) ?? CGFloat(1)
    var sizeFactor = (UserDefaults.standard.value(forKey: "sizeFactor") as? CGFloat) ?? CGFloat(0.4)
    var currentColor = (UserDefaults.standard.value(forKey: "currentColor") as? Int) ?? 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Init battery
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        self.attachSwipes()
        self.initColor()
        
        // Add listener for going to the foreground
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground(notification:)), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        // Get system locale
        locale = "\(langCode)-\(regionCode)"
        
        // Init time format
        timeFormatter.locale = Locale(identifier: locale)
        timeFormatter.dateStyle = .none
        timeFormatter.timeStyle = .short
        
        // Init date format
        dateFormatter.locale = Locale(identifier: locale)
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        
        // Init number formatter
        batteryFormatter.numberStyle = .percent
        
        // Set movement constant
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        moveConstant = CGFloat(screenHeight * 0.3)
        
        // Add tap recognition to labels
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.nextColor))
        self.view.addGestureRecognizer(tap)

        // Start
        initFromSettings()
        self.startTimers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func applicationWillEnterForeground(notification: NSNotification) {
        initFromSettings()
        updateTime()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    private func initFromSettings() -> Void {
        sizeFactor = (UserDefaults.standard.value(forKey: "sizeFactor") as? CGFloat) ?? CGFloat(0.4)
        alpha = (UserDefaults.standard.value(forKey: "alpha") as? CGFloat) ?? CGFloat(1)
        let fontSize = CGFloat(maxSize) * sizeFactor;
        updateFontSizes(size: fontSize)
        updateFontAlpha(alpha: alpha)
    }
    
    // Handle the new size of a screen after rotation
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        moveConstant = CGFloat(size.height * 0.3)
        let time = Date()
        moveTime(time: time)
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    func is24h() -> Bool {
        return (UserDefaults.standard.value(forKey: "force_24h") as? Bool) ?? true
    }
    
    func initColor() -> Void {
        setColor(colorId: currentColor)
    }
    
    @objc func nextColor() -> Void {
        currentColor = (currentColor + 1) % colors.count
        UserDefaults.standard.set(currentColor, forKey: "currentColor")
        setColor(colorId: currentColor)
    }
    
    func setColor(colorId: Int) {
        var colorToUse = "#FFFFFF"
        if (colorId < colors.count) {
            colorToUse = colors[colorId]
        }
        let col = hexStringToUIColor(hex: colorToUse)
        timeLabel.textColor = col
        dateLabel.textColor = col
        batteryLabel.textColor = col
    }
    
    func attachSwipes() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        let pinch = UIPinchGestureRecognizer(target:self, action: #selector(handlePinch));
        self.view.addGestureRecognizer(pan);
        self.view.addGestureRecognizer(pinch);
    }

    func startTimers() -> Void {
        // First run
        updateTime()
        clockTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
    }

    //The main updater loop
    @objc func updateTime() -> Void {
        let time = Date()
        if (is24h()) {
            timeFormatter.dateFormat = "HH:mm"
        } else {
            timeFormatter.dateFormat = "h:mm a"
        }
        timeLabel.text = timeFormatter.string(from: time);
        let showDate = (UserDefaults.standard.value(forKey: "show_date") as? Bool) ?? false
        let showBatt = (UserDefaults.standard.value(forKey: "show_battery") as? Bool) ?? false
        dateLabel.isHidden = !showDate
        batteryLabel.isHidden = !showBatt
        // Move the view
        if (showDate) {
            dateLabel.text = dateFormatter.string(from: time);
        }
        if (showBatt) {
            var battF = UIDevice.current.batteryLevel
            if (battF == -1) {
                battF = 0.42
            }
            let batt = batteryFormatter.string(from: NSNumber(value: battF))
            batteryLabel.text = batt
        }
        moveTime(time: time)
    }
    
    @objc func moveTime(time: Date) -> Void {
        // Calculate where we are in the cycle
        let calendar = Calendar.current
        let hours = calendar.component(.hour, from: time)
        let minutes = calendar.component(.minute, from: time)
        let seconds = calendar.component(.second, from: time)
        
        // Going up or down?
        let hourConstant = (hours % 2 == 0) ? 1 : -1
        
        // Calculate the offset based on where we are in the hourly cycle
        let offset = CGFloat(((minutes * 60) + seconds) - 1800)
        let percentOffset = CGFloat(offset / 1800)
        let pointsOffset = percentOffset * moveConstant * CGFloat(hourConstant)
        
        let intPointsOffset = Int(pointsOffset)

        if(intPointsOffset != Int(self.verticalConstraint.constant)) {
            // Move the constraint
            self.verticalConstraint.constant = pointsOffset
            // Animate
            UIView.animate(withDuration: 1) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func handlePinch(gesture: UIPinchGestureRecognizer) -> Void {
        let state = gesture.state.rawValue;
        let oldFactor = (UserDefaults.standard.value(forKey: "sizeFactor") as? CGFloat) ?? CGFloat(0.4)
        let scale = CGFloat(gesture.scale);
        var newFactor = oldFactor * scale;
        newFactor = min(newFactor, 0.75);
        newFactor = max(newFactor, 0.1);
        if (state == 2) { // Changed
            let fontSizeNew = newFactor * CGFloat(maxSize);
            updateFontSizes(size: fontSizeNew)
        }
        if (state == 3) // Ended gesture, so we save
        {
            UserDefaults.standard.set(newFactor, forKey: "sizeFactor");
            UserDefaults.standard.synchronize()
        }
    }
    
    // Set opacity of text
    @objc func handlePan(gesture: UIPanGestureRecognizer) -> Void {
        let state = gesture.state.rawValue;
        let points = gesture.translation(in: self.view)
        let y = CGFloat(points.y)
        let screenSize = UIScreen.main.bounds
        let screenHeight = CGFloat(screenSize.height)
        let percentY = y / screenHeight
        var alpha = 0.5 + (percentY * 2 * -1);
        alpha = min(1, alpha)
        alpha = max(0.1, alpha)
        updateFontAlpha(alpha: alpha)
        if (state == 3) {
            UserDefaults.standard.set(alpha, forKey: "alpha")
            UserDefaults.standard.synchronize()
        }
    }
    
    // Set all font sizes
    private func updateFontSizes(size: CGFloat) -> Void {
        let smallSize = size * CGFloat(smallTextFactor)
        timeLabel.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.thin);
        batteryLabel.font = UIFont.systemFont(ofSize: smallSize, weight: UIFont.Weight.thin);
        dateLabel.font = UIFont.systemFont(ofSize: smallSize, weight: UIFont.Weight.thin);
    }
    
    // Set label alpha values
    private func updateFontAlpha(alpha: CGFloat) -> Void {
        var actualAlpha = alpha
        // Envelope it
        actualAlpha = min(1, actualAlpha)
        actualAlpha = max(0.1, actualAlpha)
        timeLabel.alpha = alpha;
        batteryLabel.alpha = alpha
        dateLabel.alpha = alpha
    }
 
     func hexStringToUIColor (hex:String) -> UIColor {
         var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
         if (cString.hasPrefix("#")) {
         cString.remove(at: cString.startIndex)
         }
        
         if ((cString.count) != 6) {
         return UIColor.gray
         }
        
         var rgbValue:UInt32 = 0
         Scanner(string: cString).scanHexInt32(&rgbValue)
        
         return UIColor(
         red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
         green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
         blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
         alpha: CGFloat(1.0)
         )
     }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

